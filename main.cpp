#include <windows.h>
#include <iostream>

#include "glad_wgl.h"
#include <format>
#include <cassert>
#include <string>
#include <sstream>

HWND create_dummy_window()
{
    WNDCLASSEXW wndclass = {};

    wndclass.cbSize = sizeof(wndclass);
    wndclass.lpszClassName = L"wglchoose";
    wndclass.lpfnWndProc = DefWindowProcW;
    wndclass.style = CS_OWNDC;
    wndclass.hInstance = GetModuleHandle(NULL);
    if (!RegisterClassExW(&wndclass))
    {
        std::cerr << "Failed RegisterClassW" << std::endl;
        return NULL;
    }

    HWND hwnd = CreateWindowExW(0, wndclass.lpszClassName, NULL, WS_OVERLAPPED, 0, 0, 100, 100, NULL, NULL, NULL, NULL);
    if (!hwnd)
    {
        std::cerr << "Failed CreateWindowExW";
        return NULL;
    }

    return hwnd;
}

void set_dummy_pixel_format(HDC hdc)
{
    PIXELFORMATDESCRIPTOR pfd = {};
    pfd.nSize = sizeof(pfd);
    pfd.nVersion = 1;
    pfd.dwFlags = PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER | PFD_DRAW_TO_WINDOW;
    pfd.iPixelType = PFD_TYPE_RGBA;
    pfd.cColorBits = 32;
    pfd.iLayerType = PFD_MAIN_PLANE;

    int fmt = ChoosePixelFormat(hdc, &pfd);
    if (!SetPixelFormat(hdc, fmt, &pfd))
        std::cerr << "Failed to set pixel format " << fmt << std::endl;
    else
        std::cout << "Set pixel format " << fmt << std::endl;
}


void print_pixel_format(HDC hdc, int fmt)
{
    int attribs[] = {
        WGL_DRAW_TO_WINDOW_ARB, WGL_DRAW_TO_BITMAP_ARB, WGL_ACCELERATION_ARB,
        WGL_NEED_PALETTE_ARB, WGL_NEED_SYSTEM_PALETTE_ARB, WGL_SWAP_LAYER_BUFFERS_ARB,
        WGL_SWAP_METHOD_ARB, WGL_NUMBER_OVERLAYS_ARB, WGL_NUMBER_UNDERLAYS_ARB,
        WGL_TRANSPARENT_ARB, WGL_TRANSPARENT_RED_VALUE_ARB, WGL_TRANSPARENT_GREEN_VALUE_ARB,
        WGL_TRANSPARENT_BLUE_VALUE_ARB, WGL_TRANSPARENT_ALPHA_VALUE_ARB, WGL_TRANSPARENT_INDEX_VALUE_ARB,
        WGL_SHARE_DEPTH_ARB, WGL_SHARE_STENCIL_ARB, WGL_SHARE_ACCUM_ARB,
        WGL_SUPPORT_GDI_ARB, WGL_SUPPORT_OPENGL_ARB,
        WGL_DOUBLE_BUFFER_ARB, WGL_STEREO_ARB, WGL_PIXEL_TYPE_ARB,
        WGL_COLOR_BITS_ARB, WGL_RED_BITS_ARB, WGL_RED_SHIFT_ARB,  WGL_GREEN_BITS_ARB, WGL_GREEN_SHIFT_ARB,
        WGL_BLUE_BITS_ARB, WGL_BLUE_SHIFT_ARB, WGL_ALPHA_BITS_ARB, WGL_ALPHA_SHIFT_ARB,
        WGL_ACCUM_BITS_ARB, WGL_ACCUM_RED_BITS_ARB, WGL_ACCUM_GREEN_BITS_ARB,
        WGL_ACCUM_BLUE_BITS_ARB, WGL_ACCUM_ALPHA_BITS_ARB,
        WGL_DEPTH_BITS_ARB, WGL_STENCIL_BITS_ARB, WGL_AUX_BUFFERS_ARB,
    };
    int values[64];
    int nattribs = sizeof(attribs) / sizeof(*attribs);

    wglGetPixelFormatAttribivARB(hdc, fmt, 0, sizeof(attribs) / sizeof(*attribs), attribs, values);

    auto value = [nattribs, attribs, values](int a) -> int {
        for (int i = 0; i < nattribs; i++)
        {
            if (attribs[i] == a) return values[i];
        }
        assert(false);
        return -1;
        };

    bool rgba = value(WGL_PIXEL_TYPE_ARB) == WGL_TYPE_RGBA_ARB;
    bool transparent = value(WGL_TRANSPARENT_ARB);
    std::string str =
        std::format(
            "{:3} {}{} {:>9} {} {} {} {} {:3}:{:2},{:2},{:2},{:2} d{:<2} s{:<2} {:3}:{:2},{:2},{:2},{:2} aux{} {} {} {}:{:0>2x}{:0>2x}{:0>2x}{:0>2x}/{:<4}",
            fmt,
            value(WGL_DRAW_TO_WINDOW_ARB) ? "w" : " ", value(WGL_DRAW_TO_BITMAP_ARB) ? "b" : " ",
            value(WGL_ACCELERATION_ARB) == WGL_NO_ACCELERATION_ARB ? "noaccel" :
              value(WGL_ACCELERATION_ARB) == WGL_GENERIC_ACCELERATION_ARB ? "genaccel" :
              value(WGL_ACCELERATION_ARB) == WGL_FULL_ACCELERATION_ARB ? "fullaccel" : "?",
            value(WGL_SUPPORT_GDI_ARB) ? "gdi" : "   ", value(WGL_SUPPORT_OPENGL_ARB) ? "gl" : "  ",
            rgba ? "rgba" : "cidx",
            value(WGL_DOUBLE_BUFFER_ARB) ? "double" : "single",
            value(WGL_COLOR_BITS_ARB), rgba ? value(WGL_RED_BITS_ARB) : 0,
            rgba ? value(WGL_GREEN_BITS_ARB) : 0, rgba ? value(WGL_BLUE_BITS_ARB) : 0,
            rgba ? value(WGL_ALPHA_BITS_ARB) : 0,
            value(WGL_DEPTH_BITS_ARB), value(WGL_STENCIL_BITS_ARB),
            value(WGL_ACCUM_BITS_ARB), value(WGL_ACCUM_RED_BITS_ARB), value(WGL_ACCUM_GREEN_BITS_ARB),
            value(WGL_ACCUM_BLUE_BITS_ARB), value(WGL_ACCUM_ALPHA_BITS_ARB),
            value(WGL_AUX_BUFFERS_ARB),
            value(WGL_STEREO_ARB) ? "stereo" : "  mono",
            value(WGL_SWAP_METHOD_ARB) == WGL_SWAP_COPY_ARB ? "swap:copy" :
              value(WGL_SWAP_METHOD_ARB) == WGL_SWAP_EXCHANGE_ARB ? "swap:xchg" :
              value(WGL_SWAP_METHOD_ARB) == WGL_SWAP_UNDEFINED_ARB ? "swap:undf" : "swap:????",
            transparent ? "t" : "_",
            transparent ? value(WGL_TRANSPARENT_RED_VALUE_ARB) : 0,
            transparent ? value(WGL_TRANSPARENT_GREEN_VALUE_ARB) : 0,
            transparent ? value(WGL_TRANSPARENT_BLUE_VALUE_ARB) : 0,
            transparent ? value(WGL_TRANSPARENT_ALPHA_VALUE_ARB) : 0,
            transparent ? value(WGL_TRANSPARENT_INDEX_VALUE_ARB) : 0
            );
    std::cout << str << std::endl;
}

void print_all_pixel_formats(HDC hdc)
{
    int num_formats_attrib = WGL_NUMBER_PIXEL_FORMATS_ARB;
    int num_formats = 0;

    wglGetPixelFormatAttribivARB(hdc, 0, 0, 1, &num_formats_attrib, &num_formats);

    for (int fmt = 1; fmt <= num_formats; fmt++)
        print_pixel_format(hdc, fmt);

}
struct ChooseAttribs
{
    int buf = -1, r = -1, g = -1, b = -1, a = -1, d = -1, s = -1, accum = -1;
    int win = -1, bitmap = -1, db = -1, accel = -1, opengl = -1, gdi = -1, pixel = -1;
    int aux = -1, swap = -1;

    ChooseAttribs(std::string s)
    {
        std::istringstream iss{ s };
        std::string attrib;

        while (std::getline(iss, attrib, ':'))
        {
            auto tuple = parse_choose_attrib(attrib);
            auto k = std::get<0>(tuple);
            auto v = std::get<1>(tuple);

            if (k == "r" || k == "red") r = std::stoi(v);
            else if (k == "g" || k == "green") g = std::stoi(v);
            else if (k == "b" || k == "blue") b = std::stoi(v);
            else if (k == "a" || k == "alpha") a = std::stoi(v);
            else if (k == "d" || k == "depth") d = std::stoi(v);
            else if (k == "s" || k == "stencil") this->s = std::stoi(v);
            else if (k == "buf") buf = std::stoi(v);
            else if (k == "accum") accum = std::stoi(v);
            else if (k == "aux") aux = std::stoi(v);
            else if (k == "win") win = 1;
            else if (k == "!win") win = 0;
            else if (k == "bitmap") bitmap = 1;
            else if (k == "!bitmap") bitmap = 0;
            else if (k == "double") db = 1;
            else if (k == "!double") db = 0;
            else if (k == "accel")
            {
                if (v == "no") accel = WGL_NO_ACCELERATION_ARB;
                else if (v == "generic") accel = WGL_GENERIC_ACCELERATION_ARB;
                else if (v == "full") accel = WGL_FULL_ACCELERATION_ARB;
                else throw std::runtime_error("Invalid value to accel option");
            }
            else if (k == "opengl") opengl = 1;
            else if (k == "!opengl") opengl = 0;
            else if (k == "gdi") gdi = true;
            else if (k == "!gdi") gdi = false;
            else if (k == "pixel")
            {
                if (v == "rgba") pixel = WGL_TYPE_RGBA_ARB;
                else if (v == "colorindex") pixel = WGL_TYPE_COLORINDEX_ARB;
                else throw std::runtime_error("Invalid value to pixel option");
            }
            else if (k == "swap")
            {
                if (v == "exchange") swap = WGL_SWAP_EXCHANGE_ARB;
                else if (v == "copy") swap = WGL_SWAP_COPY_ARB;
                else if (v == "undefined") swap = WGL_SWAP_UNDEFINED_ARB;
                else throw std::runtime_error("Invalid value to swap option");
            }
            else if (k != "nil")
            {
                throw std::runtime_error("Unknown config option");
            }
        }
    }

    void print()
    {
        auto attribs = std::format("win:{} bitmap:{} accel:{} gdi:{} gl:{} pixel:{} db:{} {:3}:{:2},{:2},{:2},{:2} d:{:<2} s:{:<2} accum:{:<3}",
            win, bitmap, accel, gdi, opengl, pixel, db, buf, r, g, b, a, d, s, accum);
        std::cout << attribs << std::endl;
    }
private:
    std::tuple<std::string, std::string> parse_choose_attrib(std::string s)
    {
        std::istringstream iss{ s };
        std::string k, v;
        std::getline(iss, k, '=');
        std::getline(iss, v);
        return { k, v };
    }
};

void choose_pixel_format(HDC hdc, ChooseAttribs& attribs)
{
    PIXELFORMATDESCRIPTOR desc;
    memset(&desc, 0, sizeof(desc));
    desc.nSize = sizeof(desc);
    desc.nVersion = 1;
    if (attribs.opengl == 1) desc.dwFlags |= PFD_SUPPORT_OPENGL;
    if (attribs.gdi == 1) desc.dwFlags |= PFD_SUPPORT_GDI;
    if (attribs.win == 1) desc.dwFlags |= PFD_DRAW_TO_WINDOW;
    if (attribs.bitmap == 1) desc.dwFlags |= PFD_DRAW_TO_BITMAP;
    if (attribs.db < 0) desc.dwFlags |= PFD_DOUBLEBUFFER_DONTCARE;
    else if (attribs.db == 1) desc.dwFlags |= PFD_DOUBLEBUFFER;
    if (attribs.d < 0) desc.dwFlags |= PFD_DEPTH_DONTCARE;
    if (attribs.pixel == WGL_TYPE_COLORINDEX_ARB) desc.iPixelType = PFD_TYPE_COLORINDEX;
    else desc.iPixelType = PFD_TYPE_RGBA;

    if (attribs.buf >= 0) desc.cColorBits = attribs.buf;
    if (attribs.a >= 0) desc.cAlphaBits = attribs.a;
    if (attribs.d >= 0) desc.cDepthBits = attribs.d;
    if (attribs.s >= 0) desc.cStencilBits = attribs.s;
    if (attribs.aux >= 0) desc.cAuxBuffers = attribs.aux;
    if (attribs.accum >= 0) desc.cAccumBits = attribs.accum;
    
    const int choose_fmt = ::ChoosePixelFormat(hdc, &desc);
    std::cout << "--- ChoosePixelFormat ---" << std::endl;
    print_pixel_format(hdc, choose_fmt);
    std::cout << "--- wglChoosePixelFormatARB ---" << std::endl;

    int wgl_attribs[64] = { 0 };

    int* attribIter = wgl_attribs;
#define ADD_ATTRIB(attrib, name) \
    if (attribs.attrib >= 0) \
    {  \
      attribIter[0] = name; \
      attribIter[1] = attribs.attrib; \
      attribIter += 2; \
    }

    ADD_ATTRIB(win, WGL_DRAW_TO_WINDOW_ARB);
    ADD_ATTRIB(bitmap, WGL_DRAW_TO_BITMAP_ARB);
    ADD_ATTRIB(db, WGL_DOUBLE_BUFFER_ARB);
    ADD_ATTRIB(accel, WGL_ACCELERATION_ARB);
    ADD_ATTRIB(opengl, WGL_SUPPORT_OPENGL_ARB);
    ADD_ATTRIB(gdi, WGL_SUPPORT_GDI_ARB);
    ADD_ATTRIB(buf, WGL_COLOR_BITS_ARB);
    ADD_ATTRIB(pixel, WGL_PIXEL_TYPE_ARB);
    ADD_ATTRIB(r, WGL_RED_BITS_ARB);
    ADD_ATTRIB(g, WGL_GREEN_BITS_ARB);
    ADD_ATTRIB(b, WGL_BLUE_BITS_ARB);
    ADD_ATTRIB(a, WGL_ALPHA_BITS_ARB);
    ADD_ATTRIB(d, WGL_DEPTH_BITS_ARB);
    ADD_ATTRIB(s, WGL_STENCIL_BITS_ARB);
    ADD_ATTRIB(aux, WGL_AUX_BUFFERS_ARB);
    ADD_ATTRIB(swap, WGL_SWAP_METHOD_ARB);

    int formats[512];
    unsigned int num_formats = 0;

    wglChoosePixelFormatARB(hdc, wgl_attribs, NULL, 512, formats, &num_formats);
    for (unsigned int i = 0; i < num_formats; ++i)
        print_pixel_format(hdc, formats[i]);

#undef ADD_ATTRIB

}

int main(int argc, char** argv)
{
    HWND hwnd = create_dummy_window();
    HDC hdc = GetDC(hwnd);
    set_dummy_pixel_format(hdc);
    
    HGLRC ctx = wglCreateContext(hdc);
    wglMakeCurrent(hdc, ctx);
    gladLoadWGL(hdc);
    gladLoadGL();

    std::cout << "GL_RENDERER: " << glGetString(GL_RENDERER) << std::endl;
    if (argc == 1)
    {
        print_all_pixel_formats(hdc);
    }
    else
    {
        ChooseAttribs attribs{ std::string{argc > 1 ? argv[1] : ""} };
        attribs.print();
        choose_pixel_format(hdc, attribs);
    }
}
