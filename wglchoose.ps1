$Bin = ".\x64\Debug\wglchoose.exe"

function Run-Wglchoose {
	param ($ChooseParam)
	"===== wglchoose $ChooseParam ====="
	& $Bin $ChooseParam
} 

Run-Wglchoose
Run-Wglchoose nil
Run-WglChoose d=0
Run-WglChoose d=1
Run-WglChoose d=16
Run-WglChoose s=0
Run-WglChoose s=1
Run-WglChoose s=8

Run-WglChoose buf=16
Run-WglChoose buf=16:a=0
Run-WglChoose buf=16:r=5:g=5:b=5:a=1
Run-WglChoose buf=16:r=5:g=6:b=5:a=0

Run-Wglchoose buf=32
Run-WglChoose buf=32:a=0
Run-WglChoose buf=32:d=0:s=0
Run-WglChoose buf=32:d=1:s=0
Run-WglChoose buf=32:d=0:s=1
Run-Wglchoose buf=32:a=1:d=1:s=1

Run-WglChoose buf=24:a=0

Run-Wglchoose pixel=rgba:win:double:opengl:buf=32
Run-WglChoose pixel=rgba:win:double:opengl:buf=32:a=0
Run-WglChoose pixel=rgba:win:double:opengl:buf=32:d=0:s=0
Run-Wglchoose pixel=rgba:win:double:opengl:buf=32:a=1:d=1:s=1
Run-WglChoose pixel=rgba:win:double:opengl:buf=16:a=0
Run-WglChoose pixel=rgba:win:double:opengl:buf=16:r=5:g=5:b=5:a=1
Run-WglChoose pixel=rgba:win:double:opengl:buf=16:r=5:g=6:b=5:a=0